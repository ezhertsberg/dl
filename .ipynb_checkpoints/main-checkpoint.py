import random

class Preceptron:

    weights_a = 0
    weights_b = 0
    bias = 0

    def __int__(self):
        self.weights_b = random.uniform(-1, 1)
        self.weights_a = random.uniform(-1, 1)
        self.bias = random.uniform(-1, 1)

    def get_input(self, point):
        value = (point.x * self.weights_a) + (point.y * self.weights_b) + self.bias
        if value >= 0:
            return 1
        else:
            return -1

    def learn(self, result, points, lValue):
        error = points.expected - result
        self.weights_a += error * lValue
        self.weights_b += error * lValue
        self.bias += error * lValue

        print("result =", result, "target =", points.expected)
        print("weughts_a =", self.weights_a, "weights_b =", self.weights_b, "bias =", self.bias)

class points:
    x = 0
    y = 0
    expected = 0

    def __int__(self):
        self.x = random.uniform(-1, 1)
        self.y = random.uniform(-1, 1)
        self.expected = 1
        if self.x > self.y:
            self.expected = -1
        

p = Preceptron()
p.__int__()
lValue = 0.5
x = points()
x.__int__()
for i in range(10):
    print(p.get_input(x))
    p.learn(p.get_input(x), x, lValue)
    print(p.get_input(x))

