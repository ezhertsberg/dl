import gzip

import numpy as np
from PIL import Image


class train_data:
    def __init__(self):
        self.testing_photos = gzip.open("C:\\Users\\User\\Desktop\\dl\\data\\t10k-images-idx3-ubyte.gz", "rb")
        self.testing_labels = gzip.open("C:\\Users\\User\\Desktop\\dl\\data\\t10k-labels-idx1-ubyte.gz", "rb")
        self.training_photos = gzip.open("C:\\Users\\User\\Desktop\\dl\\data\\train-images-idx3-ubyte.gz", "rb")
        self.training_labels = gzip.open("C:\\Users\\User\\Desktop\\dl\\data\\train-labels-idx1-ubyte.gz", "rb")
        self.training_photos.read(16)
        self.training_labels.read(8)
        self.testing_photos.read(16)
        self.testing_labels.read(8)

    def init_training_photos(self):
        answer = np.zeros((10, 784, 1))
        for i in range(10):
            for j in range(784):
                answer[i][j][0] = int.from_bytes(self.training_photos.read(1), "big") / 255
        return answer

    def init_training_labels(self):
        answer = np.zeros((10, 10, 1))
        for i in range(10):
            answer[i][int.from_bytes(self.training_labels.read(1), "big")][0] = 1
        return answer

    def init_testing_photos(self):
        answer = np.zeros((10000, 784, 1))
        for i in range(10000):
            for j in range(784):
                answer[i][j][0] = int.from_bytes(self.testing_photos.read(1), "big") / 255
        return answer

    def init_testing_labels(self):
        answer = np.zeros((10000, 10, 1))
        for i in range(10000):
            answer[i][int.from_bytes(self.testing_labels.read(1), "big")][0] = 1
        return answer

