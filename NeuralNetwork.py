import numpy as np

from gzipTest import train_data


def sigmoid(inputs):
    return 1 / (1 + np.exp(-inputs))


def desgmoid(hidden_z):
    answer = np.copy(hidden_z)
    for i in range(len(hidden_z)):
        answer[i] = sigmoid(hidden_z[i]) - np.math.pow(sigmoid(hidden_z[i]), 2)
    return answer


class Network:
    def __init__(self, sizes, learning_rate):
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.zeros((y, 1)) + 1 for y in sizes[1:]]
        self.weights = [np.random.randn(y, x)
                        for x, y in zip(sizes[:-1], sizes[1:])]
        self.learning_rate = learning_rate
        self.sum_errors = [np.zeros((y, 1)) for y in sizes[1:]]
        self.sum_delta_weights = [np.zeros((y, x))
                                  for x, y in zip(sizes[:-1], sizes[1:])]

    def feed_forward(self, inputs):
        hidden_z = np.dot(self.weights[0], inputs)
        hidden_z = hidden_z + self.biases[0]
        hidden_a = sigmoid(hidden_z)

        output_z = np.dot(self.weights[1], hidden_a)
        output_z = output_z + self.biases[1]
        output_a = sigmoid(output_z)

        return hidden_z, hidden_a, output_z, output_a

    def train(self, inputs, targets):
        hidden_z, hidden_a, output_z, output_a = self.feed_forward(inputs)
        error = output_a - targets
        error_output = error * desgmoid(output_z)
        error_hidden = np.dot((np.transpose(self.weights[1])), error_output) * desgmoid(hidden_z)
        delta_weights_output = np.transpose(hidden_a) * error_output
        delta_weights_hidden = np.transpose(inputs) * error_hidden

        self.weights[0] -= (delta_weights_hidden * self.learning_rate)
        self.weights[1] -= (delta_weights_output * self.learning_rate)
        self.biases[0] -= (error_hidden * self.learning_rate)
        self.biases[1] -= (error_output * self.learning_rate)

    def epoch_train(self, inputs, targets):
        hidden_z, hidden_a, output_z, output_a = self.feed_forward(inputs)
        error = output_a - targets
        error_output = error * desgmoid(output_z)
        error_hidden = np.dot((np.transpose(self.weights[1])), error_output) * desgmoid(hidden_z)
        delta_weights_output = np.transpose(hidden_a) * error_output
        delta_weights_hidden = np.transpose(inputs) * error_hidden

        self.sum_errors[-1] += error_output
        self.sum_errors[-2] += error_hidden
        self.sum_delta_weights[-1] += delta_weights_output
        self.sum_delta_weights[-2] += delta_weights_hidden

    def init_train(self):
        self.biases[1] -= (self.sum_errors[-1] * self.learning_rate) / 10
        self.biases[0] -= (self.sum_errors[-2] * self.learning_rate) / 10
        self.weights[1] -= (self.sum_delta_weights[-1] * self.learning_rate) / 10
        self.weights[0] -= (self.sum_delta_weights[-2] * self.learning_rate) / 10

        self.sum_errors = [np.zeros((y, 1)) for y in self.sizes[1:]]
        self.sum_delta_weights = [np.zeros((y, x))
                                  for x, y in zip(self.sizes[:-1], self.sizes[1:])]


def comperTo(d, param):
    for i in range(len(d)):
        if d[i] != param[i]:
            return 0
    return 1


net = Network([784, 100, 10], 0.1)
for test in range(10):
    data = train_data()

    for i in range(6000):
        train_photos = data.init_training_photos()
        train_labels = data.init_training_labels()
        for j in range(10):
            net.epoch_train(train_photos[j], train_labels[j])
        net.init_train()
    z = 0
    test_photos = data.init_testing_photos()
    test_labels = data.init_testing_labels()
    for j in range(10000):
        a, b, c, d = net.feed_forward(test_photos[j])
        for answer in range(len(d)):
            if d[answer] > 0.5:
                d[answer] = 1
            else:
                d[answer] = 0
        z += comperTo(d, test_labels[j])
    print(z, "from 10000\n-------", end="\n\n")

    # inputs = [[[1], [1]],
    #           [[1], [0]],
    #           [[0], [1]],
    #           [[0], [0]]]
    #
    # target = [[[0]],
    #           [[1]],
    #           [[1]],
    #           [[0]]]
    #
    # for i in range(4400):
    #     a = random.randrange(4)
    #     for j in range(1):
    #         net.train(inputs[a], target[a])
    #
    # a, b, c, d = net.feed_forward([[1], [1]])
    # if d[0][0] > 0.5:
    #     print(1, "target = 0")
    # else:
    #     print(0, "target = 0")
    # a, b, c, d = net.feed_forward([[1], [0]])
    # if d[0][0] > 0.5:
    #     print(1, "target = 1")
    # else:
    #     print(0, "target = 1")
    # a, b, c, d = net.feed_forward([[0], [0]])
    # if d[0][0] > 0.5:
    #     print(1, "target = 0")
    # else:
    #     print(0, "target = 0")
    # a, b, c, d = net.feed_forward([[0], [1]])
    # if d[0][0] > 0.5:
    #     print(1, "target = 1")
    # else:
    #     print(0, "target = 1")
